<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $customers = Customer::all();
        
        return view('customers.index',compact('customers'));
     }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $this->validate($request,[
       //     'title'=>'required'
       // ]);
        $id = Auth::id();
        $customer=new Customer();
        $customer->user_id=$id;
        $customer->name=$request->name;
        $customer->email=$request->email;
        $customer->phone=$request->phone;
        $customer->save();
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customers.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer=Customer::findOrFail($id);
        if(!$customer->user->id ==Auth::id()) return (redirect('customers'));
        //$this->validate($request,[
        //   'title'=>'required',
        //  'updated_at'=>'required|date|before_or_equal:today']);
        
        /*if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit tasks..");
        }
          $task->update($request->except(['_token']));
        if ($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }*/
        return redirect('customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Gate::denies('manager'))
        {
            abort(403,"Sorry you are not allowed to delete tasks..");
        }
        $customer = Customer::find($id);
        if(!$customer->user->id ==Auth::id()) return (redirect('customers'));
        $customer ->delete();
        return redirect('customers');
    }
    public function done($id)
    {
        if(Gate::denies('manager'))
        {
            abort(403,"Sorry you are not allowed to deal closed..");
        }
        $customer=Customer::findOrFail($id);
        $customer->status=1;
        $customer->save();
        return redirect('customers');
    }
}
