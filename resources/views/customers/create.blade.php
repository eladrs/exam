@extends('layouts.app')
@section('content')
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class="container">
<br><br>
<h3> Add a new Customer </h3>
<!--validation errors
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif-->

<form method="post" action ="{{action('CustomersController@store')}}">
{{csrf_field()}}
<div class="form-group">
<label for="name">the customer's name :</label>
<input type ="text" class ="form-control" name="name"><br>
<label for="email">the customer's email :</label>
<input type ="text" class ="form-control" name="email"><br>
<label for="phone">the customer's phone :</label>
<input type ="text" class ="form-control" name="phone"><br>
</div>
<div class ="container">
<div class="col-5  offset-4">
    <input type ="submit" class="form-control btn btn-secondary" name="submit" value ="save"> 
</div>
</div>
<br>
        <div class ="container">
            <div class="col-5  offset-4">
                <a href="{{route('customers.index')}}" class=" form-control btn btn-secondary">Go back to the Customer list</a>
            </div>
        </div>
</div>
</form>
</div>
@endsection
