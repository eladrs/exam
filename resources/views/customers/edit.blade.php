@extends('layouts.app')
@section('content')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


    <div class ="container">
      <div class="col-2 offset-10">
      <a href = "{{route('customers.index')}}" > back to customers list</a>
      </div>
    </div>
    <!--validation errors
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif-->

<!--update-->
<div class="container">
  <h3>Update customer</h3>
    <form method="post" action ="{{action('CustomersController@update',$customer->id)}}">
     @csrf
     @method('PATCH')
        <div class="form-group">
                <label for="title">customer to update</label>
                <input type ="text" class ="form-control" name="title" value = "{{$customer->name}}">
        </div>
        <div class="form-group">
                <label for = "date" >new email </label>
                <input type="email" class = "form-control" name="updated_at" value = "{{$customer->email}}">
        </div>  
        <div class="form-group">
                <label for = "date" >new phone </label>
                <input type="text" class = "form-control" name="updated_at" value = "{{$customer->phone}}">
        </div>
        <div class ="container">
            <div class="col-4  offset-4">
                <input type ="submit" class="form-control btn btn-secondary" name="submit" value ="Save Update"> 
            </div>
        </div>
    </form>
</div><br>

@endsection

