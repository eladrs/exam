@extends('layouts.app')
@section('content')
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 

   <div class='container' >
     <div class="col-3  offset-5">
      <a href="{{route('customers.create')}}" class=" btn btn-secondary">create new customers</a>
     </div>
    </div>

<div class='container'>
<br><br>
  <table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col"> customer name</th>
      <th scope="col"> Salesman name</th>
      <th scope="col"> delete customer</th>
      <th scope="col"> email</th>
      <th scope="col"> phone</th>
      <th scope="col"> deal status</th>
    </tr>
  </thead>
  <tbody>
  @foreach($customers as $customer)

  
    <tr>
      @if($customer->status==1)
      <td bgcolor="green">{{$customer->name}}<a href = "{{route('customers.edit',$customer->id)}}" > edit</a></td>
      @else
      <td>{{$customer->name}}<a href = "{{route('customers.edit',$customer->id)}}" > edit</a></td>
      @endif
      <td>{{$customer->user->name}}</a></td>
    
      <td>@cannot('user')<form method="post" action ="{{action('CustomersController@destroy',$customer->id)}}">
      @csrf
      @method('DELETE')
      <input type ="submit" class="form-control  btn-primary" name="submit" value ="delete">
      </form> @endcannot</td>
      <td>{{$customer->email}}</td> 
      <td>{{$customer->phone}}</td>
      <td>
        @if($customer->status==0)
        @cannot('user') 
          <a href="{{route('done',$customer->id)}}">@endcannot deal closed</a>
        @else

        @endif  
     </td> 
     </tr>
  @endforeach

  </tbody>
  </table>
  <br>
  </div>
      
@endsection